<?php

/**
 * @file
 * Base class for all Administration Views web test cases.
 */
  
/**
 * Base class for all Administration Views web test cases.
 */
class AdminViewsBaseTestCase extends DrupalWebTestCase {

  /**
   * {@inheritdoc}
   */
  protected $profile = 'testing';

  /**
   * @var array
   */
  protected $permissionMap = array(
    'user' => array('administer users', 'administer permissions'),
    'node' => array('access content overview'),
    'comment' => array('administer comments'),
    'taxonomy' => array('administer taxonomy'),
  );

  /**
   * {@inheritdoc}
   */
  public function setUp(array $modules = array()) {
    // Setup site and modules.
    $modules[] = 'admin_views_system_display';
    $modules[] = 'admin_views';
    parent::setUp($modules);

    // Fix testing environment.
    theme_enable(array('stark'));
    variable_set('theme_default', 'stark');

    // Setup permissions.
    $permissions = array(
      'access administration pages',
    );
    foreach ($this->permissionMap as $module => $module_permissions) {
      if (module_exists($module)) {
        $permissions = array_merge($permissions, $module_permissions);
      }
    }
    $this->admin_user = $this->drupalCreateUser($permissions);

    // Setup default configuration.
    if (in_array('node', $modules)) {
      $this->node_type = $this->drupalCreateContentType(array(
        'type' => 'article',
        'name' => 'Article',
        // 2 == COMMENT_NODE_OPEN.
        'comment' => 2,
      ));
    }
    if (in_array('comment', $modules)) {
      variable_set('comment_preview_article', DRUPAL_OPTIONAL);
    }
  }

  /**
   * Log in as user 1.
   */
  protected function loginUser1() {
    $password = user_password();
    // Reset the user 1 password.
    $account = user_load(1);
    $edit = array(
      'pass' => $password,
    );
    $account = user_save($account, $edit);
    $account->pass_raw = $password;

    // Log in as user 1.
    $this->drupalLogin($account);
  }

  protected function assertOriginalRouterItem($module, $path) {
    // Retrieve the original router item definition.
    $items = module_invoke($module, 'menu');
    $original_item = $items[$path];
    // Retrieve the computed router item definition.
    $item = menu_get_item($path);

    // Verify that basic properties are identical.
    $title = (isset($original_item['title callback']) ? $original_item['title callback']() : $original_item['title']);
    $this->assertEqual($title, $item['title']);
    if (isset($original_item['description'])) {
      $this->assertEqual($original_item['description'], $item['description']);
    }

    // Verify that the title appears.
    $this->assertResponse(200);
    $this->assertText($original_item['title']);
  }

}
